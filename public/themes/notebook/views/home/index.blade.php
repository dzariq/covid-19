<script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.0/dist/chart.min.js"></script>
<div >
    <h3>Covid-19 Daily Cases</h3>
    <canvas id="myChart" width="400" height="200"></canvas>
</div>
<script>
    var cases = <?php echo json_encode($cases); ?>;
    var dates = <?php echo json_encode($dates); ?>;
console.log(cases)
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: dates,
            datasets: [
                     {
                    label: 'WP Putrajaya',
                    data: cases['WP PUTRAJAYA']['data'],
                    borderWidth: 1,
                    borderColor: '#0d1859'
                },
                     {
                    label: 'WP Kuala Lumpur',
                    data: cases['WP KUALA LUMPUR']['data'],
                    borderWidth: 1,
                    borderColor: '#5e5343'
                },
                     {
                    label: 'WP Labuan',
                    data: cases['WP LABUAN']['data'],
                    borderWidth: 1,
                    borderColor: '#e3b578'
                },
                  {
                    label: 'Negeri Sembilan',
                    data: cases['NEGERI SEMBILAN']['data'],
                    borderWidth: 1,
                    borderColor: '#f00c7e'
                },
                  {
                    label: 'Pulau Pinang',
                    data: cases['PULAU PINANG']['data'],
                    borderWidth: 1,
                    borderColor: '#cc39ac'
                },
                  {
                    label: 'Terengganu',
                    data: cases['TERENGGANU']['data'],
                    borderWidth: 1,
                    borderColor: '#dce7e8'
                },
                  {
                    label: 'Pahang',
                    data: cases['PAHANG']['data'],
                    borderWidth: 1,
                    borderColor: 'black'
                },
                  {
                    label: 'Perlis',
                    data: cases['PERLIS']['data'],
                    borderWidth: 1,
                    borderColor: '#8da832'
                },
                  {
                    label: 'Sabah',
                    data: cases['SABAH']['data'],
                    borderWidth: 1,
                    borderColor: '#6de9f2'
                },  {
                    label: 'Sarawak',
                    data: cases['SARAWAK']['data'],
                    borderWidth: 1,
                    borderColor: 'orange'
                },  {
                    label: 'Perak',
                    data: cases['PERAK']['data'],
                    borderWidth: 1,
                    borderColor: 'yellow'
                },  {
                    label: 'Kedah',
                    data: cases['KEDAH']['data'],
                    borderWidth: 1,
                    borderColor: 'green'
                },  
                {
                    label: 'Kelantan',
                    data: cases['KELANTAN']['data'],
                    borderWidth: 1,
                    borderColor: 'red'
                },
                {
                    label: 'Melaka',
                    data: cases['MELAKA']['data'],
                    borderWidth: 1,
                    borderColor: '#590d0e'
                },
                {
                    label: 'Johor',
                    data: cases['JOHOR']['data'],
                    borderWidth: 1,
                    borderColor: '#3e32a8'
                },
                {
                    label: 'Selangor',
                    data: cases['SELANGOR']['data'],
                    borderWidth: 1,
                    borderColor: '#a8a232'
                },
            ]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>