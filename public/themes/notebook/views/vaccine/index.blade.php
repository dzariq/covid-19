<style>
    #map {
        height: 500px;
    }

    .legend { padding-top: 20px;list-style: none; margin-left:10px;z-index: 999999;
              background: white;}
    .legend li { float: left; margin-right: 15px;}
    .legend span { border: 1px solid #ccc; float: left; width: 10px; height: 12px; margin: 2px;}
    .legend {
        position: fixed;
        top: 62%;
        left: 10%; 
        /* bring your own prefixes */
        transform: translate(-50%, -50%);
    }
    /* your colors */
    .legend .greendot { background-color: #00ff00;}
    .legend .yellowdot { background-color: #ffff33; }
    .legend .reddot { background-color: #ff3333; }
</style>
<div class="row m-t-xl m-b-xl text-center">
    <h3>Covid-19 Vaccination Status as of 13 July 2021</h3>
    <ul class="legend">
        <li><span class="greendot"></span>> 50% Vaccinated</li><br></br>
        <li><span class="yellowdot"></span>20%-50% Vaccinated </li><br></br>
        <li><span class="reddot"></span>< 20% Vaccinated</li><br></br>
    </ul>
    <div id="map" >
    </div>
</div>
<script>
    function initMap() {
        var vaccination = <?php echo json_encode($vaccination); ?>;
        console.log(vaccination)
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 4.2105, lng: 106.9758},
            zoom: 6
        });
        map.data.loadGeoJson('negeri_polygons.json');

        map.data.setStyle(function (feature) {
            var negeri = feature.getProperty('negeri');
            console.log(negeri);
            console.log(vaccination[negeri]);
            var color = 'yellow';
            if (vaccination[negeri] > 50)
                color = 'green';
            else if (vaccination[negeri] < 20)
                color = 'red';

            return {
                fillColor: color,
                strokeWeight: 1
            };
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap">
</script>