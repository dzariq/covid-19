<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Theme;

class VaccineController extends Controller {

    public function GET_index() {
        $theme = Theme::uses('notebook')->layout('landing');
        $theme->setMenu('vaccine.index');

        $theme->asset()->usePath()->add('landing', 'css/landing.css');

        $theme->asset()->container('post-scripts')->usePath()->add('laravel1', 'js/app.plugin.js');
        $theme->asset()->container('post-scripts')->usePath()->add('laravel2', 'js/scroll/smoothscroll.js');
//        $theme->asset()->container('post-scripts')->usePath()->add('laravel3', 'js/landing.js');

        $displayDate = '2021-07-13';
        $vaccination = \App\VacState::where('date', $displayDate)->get();
        $vaccinationArrayState = array();

        foreach ($vaccination as $vacc) {
            #get mapped name
            $stateModel = \App\State::find($vacc->state_id);
            
            $population = $stateModel->pop;
            $vaccinated = $vacc->total_dose_1;
            $percentageVaccinated = $vaccinated/$population * 100;
            
            $vaccinationArrayState[$stateModel->mapped_name] = $percentageVaccinated;
        }

        $params = array(
            'vaccination' => $vaccinationArrayState
        );
        return $theme->scope('vaccine.index', $params)->render();
    }

}
