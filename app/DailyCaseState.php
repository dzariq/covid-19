<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyCaseState extends Model
{
    protected $table = 'daily_case_state';

    public function __construct()
    {
    }
}
